import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author mirajul.mohin
 * @since 2/4/20
 */
public class LogParser {
    private static final int HOURS = 24;
    public static void main(String []args) throws IOException {
        FileReader fr = new FileReader("/home/mirajul.mohin/IdeaProjects/net/therap/Assignment1/src/main/java/src/log-parser-sample-file.log");
        Scanner sc = new Scanner(fr);

        int  i = 0;
        InfoOfLog[] obj = new InfoOfLog[34];

        for(int j = 0 ; j<34 ; j++)
        {
            obj[j]=new InfoOfLog();
        }

        while(sc.hasNext()) {
            int getCount=0;
            int postCount=0;
            String log = sc.nextLine();
            String[] OneLog= log.split(",");


            if(OneLog.length!=4)
            {
                continue;
            }

            String[] Uri=OneLog[1].split("=");
            String[] TypeOfLog;
            TypeOfLog = OneLog[2].split(" ");


            if(TypeOfLog.length==2)
            {
                if(TypeOfLog[1].charAt(0)=='G')
                {
                    getCount++;
                }
                if(TypeOfLog[1].charAt(0)=='P')
                {
                    postCount++;
                }

            }

            String[] Time;
            String[] hour;
            Time = OneLog[0].split(" ");
            hour = Time[1].split((":"));


            String[] resTime;
            resTime = OneLog[3].trim().split("=");


            int responseTime=0;
            if(resTime[0].equals("time"))
            {
                int len = resTime[1].length();
                responseTime= Integer.parseInt(resTime[1].substring(0,len-2));
            }


            int hr=Integer.parseInt(hour[0]);
            int gc = obj[hr].getGetCount();
            obj[hr].setGetCount(gc+getCount);


            int pc = obj[hr].getPostCount();
            obj[hr].setPostCount(pc + postCount);


            int res = obj[hr].getTotalResponseTime();
            obj[hr].setTotalResponseTime(res + responseTime);

            if(Uri.length==2) {
                obj[hr].UriCount.add(Uri[1]);
            }

        }

        System.out.println("Time"+"     "+"       GET / POST count" + "     " + " Unique URI count"+"    "+ "Total response time");

        for(int j= 0 ; j<12 ; j++)
        {
            if(j%HOURS==0)
            {
                System.out.print("12:00 am - 1:00 am");
            }
            else
            {
                System.out.print(j%HOURS +".00 am - "+((j%HOURS)+1)+".00 am");
            }
            obj[j].printLog();
        }


        for(int j= 12 ; j<24 ; j++)
        {
            if(j%12==0)
            {
                System.out.print("12:00 pm - 1:00 pm");
            }
            else
            {
                System.out.print(j%12 +".00 pm - "+((j%12)+1)+".00 pm");
            }
            obj[j].printLog();
        }

    }
}
