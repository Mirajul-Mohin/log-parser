import java.util.HashSet;
import java.util.Set;

/**
 * @author mirajul.mohin
 * @since 2/4/20
 */
public class InfoOfLog {

    private int getCount;
    private int postCount;
    private int uniqueURI;
    private int totalResponseTime;
    Set<String> UriCount=new HashSet<String>();

    public int getGetCount() {
        return getCount;
    }

    public void setGetCount(int getCount) {
        this.getCount = getCount;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public int getUniqueURI() {
        return uniqueURI;
    }

    public void setUniqueURI(int uniqueURI) {
        this.uniqueURI = uniqueURI;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }

    public void printLog()
    {
        System.out.println("      "+getCount + "/" + postCount + "             " + UriCount.size() + "                   " + totalResponseTime);
    }


}
